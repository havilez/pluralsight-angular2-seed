import {Component, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';


import { Employee } from '../models/employee.model'
import { FormPoster } from '../services/form.poster.service';


@Component({
  selector: 'home',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  languages = [];
  model = new Employee('Daria','Smith',true, 'w2','default');
  hasPrimaryLanguageError = true;

  startDate = new Date();
  mytime= new Date();
  onOffSwitch: any = 'On';
  taxType: string = 'W2';
  postRating: number=5;


  constructor(private formPoster: FormPoster) {
    // FIX-ME: call should be in ngInit or use async pipe
    this.formPoster.getLanguages()
        .subscribe(
          data => this.languages = data.languages,
          err => console.log('GET error: ',err)
        );
  }

  ngOnInit() {

  }

  validatePrimaryLanguage(value)  {

    if ( value === 'default') {
      this.hasPrimaryLanguageError = true;
    }
    else
       this.hasPrimaryLanguageError = false;


  }


  // set first character of string to upper case
  firstNameToUpperCase(event: any) {
    let value = event.target.value;
    console.log(event.target.value);
    if (value.length > 0 )
      this.model.firstName = value.charAt(0).toUpperCase() + value.slice(1)
    else
      this.model.firstName = value;

  }

  submitForm( form: NgForm ) {

    console.log('Model = ', this.model);
    console.log('Form data = ', form.value);

    // validate form
    if ( this.validatePrimaryLanguage(this.model.primaryLanguage) )
      return;

    this.formPoster.postEmployeeForm(this.model)
        .subscribe(
          data => console.log('success: ', data),
          err => console.log('error: ', err)
        )
    }



};  // end class
